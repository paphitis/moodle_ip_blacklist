<?php
/**
 * IP blacklist plugin for Moodle
 *
 * Blacklist IP addresses that can login
 *
 * @package auth_ip_blacklist
 * @author Alex Paphitis <alex@paphitis.net>
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/authlib.php');

/**
 * Plugin to blacklist list of IP addresses from logging in
 */
class auth_plugin_ip_blacklist extends auth_plugin_base {
    /**
     * Constructor.
     */
    public function __construct() {
        $this->authtype = 'ip_blacklist';
        $this->config = (array) get_config('auth_ip_blacklist');
        $this->blacklisted = $this->getBlacklist();
    }

    /**
     *  Method that's called before the login page loads
     */
    public function pre_loginpage_hook() {
        $this->loginpage_hook();
    }

    /**
     *  Method that's called after the login page loads
     */
    public function loginpage_hook() {
        $userAddress = getremoteaddr();

        if (!isset($userAddress)) {
            error_log("User IP address is not set");
            $this->unauthorized();
            return false;
        }

        if ($this->isBlacklistedIP($userAddress))
            $this->unauthorized();
            return false;
    }

    /**
     * Get the blacklist as an array
     *
     * @return array The array of blacklisted IP addresses and CIDRs
     */
    private function getBlacklist() {
        $blacklistStr = $this->config["blacklisted"];
        $blacklistStr = str_replace(" ", "", $blacklistStr); // Remove whitespace
        return explode(",", $blacklistStr); // Return as an array
    }

    /**
     * Redirect to a 401 unauthorized page
     *
     * @param string $msg Optional message that can also be displayed
     *
     */
    private function unauthorized($msg = null) {
        header('HTTP/1.0 401 Unauthorized');
        print new lang_string('unauthorized', 'auth_ip_blacklist');

        if (isset($msg))
            print ": " . $msg;

        exit;
    }

    /**
     * Determine if the given IP address is blacklisted
     *
     * @param $ipAddress The IP address to check against the blacklist
     * @return bool Whether the IP address is blacklisted or not
     */
    function isBlacklistedIP($ipAddress) {
        // Loop over each address or range in the array of blacklisted array
        // to see if the IP address given is in it
        foreach ($this->blacklisted as $b) {
            if ($b == $ipAddress)
                return true;
            if ($this->isCIDRNotation($b) && $this->isIPInRange($b, $ipAddress))
                return true;
        }

        return false;
    }

    /**
     * Check if a given IP address is within an IP range
     *
     * @param string $cidr The CIDR to check against
     * @param string $ipAddress The IP address to check
     * @return bool Whether the IP address is within range
     */
    function isIPInRange($cidr, $ipAddress) {
        list($subnet, $mask) = explode('/', $cidr);

        if ((ip2long($ipAddress) & ~((1 << (32 - $mask)) - 1) ) == ip2long($subnet))
            return true;

        return false;
    }

    /**
     * Check if the given string is a CIDR
     *
     * @param string $str The string to test
     * @return bool Whether the string is a CIDR or not
     */
    function isCIDRNotation($str) {
        // Check for slash
        if (strpos($str, "/") !== false)
            return false;

        $dotCount = substr_count($str, ".");

        // Split into two parts, one with the IP and one with the prefix
        // Validate both parts separately
        $parts = explode("/", $str);

        $ip = $parts[0];
        $prefix = $parts[1];

        if ($ip < 0 || $prefix < 0)
            return false;

        $ipv4 = filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        $ipv6 = filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);

        if (!$ipv4 && !$ipv6)
            return false;

        if ($ipv4 && ($prefix > 29))
            return false;
        else if ($ipv6 && ($prefix > 64))
            return false;

        return true;
    }

    /**
     * Don't handle the user_login part
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Always return false
     *
     */
    public function user_login($username, $password) {
        return false;
    }
}