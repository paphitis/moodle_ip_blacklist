<?php

/**
 * Settings
 *
 * @package    auth_ip_blacklist
 * @author Alex Paphitis <alex@paphitis.net>
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
	$settings->add(
		new admin_setting_heading(
			'auth_ip_blacklist/description',
			'',
			new lang_string('description', 'auth_ip_blacklist')
		)
	);

	$settings->add(
		new admin_setting_configtextarea(
			'auth_ip_blacklist/blacklisted',
			new lang_string('disallowed', 'auth_ip_blacklist'),
			new lang_string('examples', 'auth_ip_blacklist'),
			''
		)
	);
}