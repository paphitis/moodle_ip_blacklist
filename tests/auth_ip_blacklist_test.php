<?php
/**
 * Tests for IP blacklist plugin
 *
 * @package auth_ip_blacklist
 * @author Alex Paphitis <alex@paphitis.net>
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot . '/auth/ip_blacklist/auth.php');

class auth_ip_blacklist_testcase extends advanced_testcase {
    protected $authplugin;

    protected function setUp() {
        $this->authplugin = new auth_plugin_ip_blacklist();
    }
}
