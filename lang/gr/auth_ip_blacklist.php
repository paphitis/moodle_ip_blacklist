<?php
/**
 * Greek strings
 *
 * @package    auth_ip_blacklist
 * @author     Alex Paphitis <alex@paphitis.net>
 */

$string['pluginname'] = 'IP μαύρη λίστα';
$string['description'] = 'Περιορίστε τη σύνδεση με τη διεύθυνση IP';
$string['disallowed'] = 'Δεν επιτρέπονται διευθύνσεις IP';
$string['examples'] = 'Οι διευθύνσεις IP χωρίζονται με κόμματα. Παραδείγματα: 127.0.0.1, 192.168.1.1';
$string['unauthorized'] = 'Μη εξουσιοδοτημένη διεύθυνση IP';