<?php
/**
 * English strings
 *
 * @package    auth_ip_blacklist
 * @author     Alex Paphitis <alex@paphitis.net>
 */

$string['pluginname'] = 'IP blacklist';
$string['description'] = 'Restrict logging in by IP address';
$string['disallowed'] = 'Disallowed IP addresses';
$string['examples'] = 'IP Addresses separated by commas. Examples: 127.0.0.1, 192.168.1.1';
$string['unauthorized'] = 'Unauthorized IP Address';