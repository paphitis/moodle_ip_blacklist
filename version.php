<?php
/**
 * Versioning
 *
 * @package    auth_ip_blacklist
 * @author     Alex Paphitis <alex@paphitis.net>
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version = 2019101401;
$plugin->requires = 2015051101;
$plugin->component = 'auth_ip_blacklist';
$plugin->release = '1.0.0';
$plugin->maturity = MATURITY_STABLE;